Autor: Adrian Kudzinśki
Nazwa gry: .: AnimeCard :.

PL:
Gra opiera się na kartach z potaciami z popularnych japońskich animacji.
Losowo zdobywasz 5 kart, które mogą zmienić się w super kartę (złotą lub platynową) i doprowadzić Cię do zwycięstwa!
Każda karta ma swój zakres mocy, z którego losowana jest wartość decydująca o przyznaniu punktu.
Po wygranej turze, karty kolejno zamieniają się w super kartę.
Jeśli gracz jako pierwszy uzyska 5 złotych kart, wygrywa grę.

EN:
Game is based on anime characters.
You earn 5 random cards that can turn into a super card (gold or platinum), and lead you to victory!
Each card has a range from which is drawn value. If your power value is bigger than opponent's power, you get a point.
After winning the round, the cards sequentially turn into a super card.
If a player is the first to receive 5 gold cards, he win.