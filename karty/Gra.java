package karty;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import javax.swing.JPanel;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import javax.imageio.ImageIO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;

import javax.swing.border.EmptyBorder;
import javax.swing.text.html.HTMLDocument.Iterator;

import java.net.URL;

import java.awt.image.BufferStrategy;

public class Gra extends Canvas{

	public BufferedImage loadImage(String sciezka) {
		URL url = null;
		try {
			url = getClass().getClassLoader().getResource(sciezka);
			return ImageIO.read(url);
		} catch (Exception e) {
			System.out.println("Przy otwieraniu " + sciezka +" jako " + url);
			System.out.println("Wystapil blad : "+e.getClass().getName()+e.getMessage());
					System.exit(0);
			return null;
		}
	}
	
	public BufferedImage getSprite(String sciezka) {
		BufferedImage img = (BufferedImage)sprites.get(sciezka);
		if(img == null) {
			img = loadImage("img/"+sciezka);
			sprites.put(sciezka,img);
		}
		return img;
	}

	public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
	    Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();

	    return dimg;
	}  
	
	public static final int SZEROKOSC = 800;
	public static final int WYSOKOSC = 600;
	public static int SZYBKOSC = 60;
	public BufferStrategy strategia;
	public int com_pkts = 0;
	public int player_pkts = 0;
	public int wygrana = 0;
	public int player_win = 0;
	public int com_win = 0;
	boolean zlota1, zlota2, zlota3, zlota4, zlota5 = false;
	
	public static ArrayList<Karta> talia1 = new ArrayList<>();
	public static ArrayList<Karta> talia2 = new ArrayList<>();
	public static ArrayList<Karta> talia3 = new ArrayList<>();
	public static ArrayList<Karta> talia4 = new ArrayList<>();
	public static ArrayList<Karta> talia5 = new ArrayList<>();
	
	public ArrayList<Karta> com_talia = new ArrayList<>();
	public ArrayList<Karta> player_talia = new ArrayList<>();
	
	int rysuj = 0;
	
	public static HashMap sprites;
	
	//Wypisuje statystyki wybranej karty z wybranej talii, wykorzystuje wyjatek
	public static void wypiszKarteZTalii(ArrayList<Karta> talia, int ktoraKarta) throws NonExistCardException {
		if(ktoraKarta < 0 || ktoraKarta > talia.size()) {
			throw new NonExistCardException();
		}
		else
		{
			System.out.println("karta nr " + ktoraKarta + " : " + talia.get(ktoraKarta));
		}
	}

	public Gra() {
		//Wczytanie pieciu talii kart i losowanie
		wczytajTalie(1, talia1);
		wczytajTalie(2, talia2);
		wczytajTalie(3, talia3);
		wczytajTalie(4, talia4);
		wczytajTalie(5, talia5);
		losujKarte();
		
		sprites = new HashMap();
		JFrame okno = new JFrame(".: Anime Cards :.");
		JPanel panel = (JPanel)okno.getContentPane();
		JButton button = new JButton("Walcz!");
		button.setBounds(307, 370, 170, 80);
		button.setLayout(null);
		panel.add(button);
		button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	//Etapy gry
				if(com_win == 5){
					JOptionPane.showMessageDialog(null, "Gra jest zakonczona. Przegrales.");
				}
				else if(player_win == 5){
					JOptionPane.showMessageDialog(null, "Gra jest zakonczona. Wygrales.");
				}
				else if(rysuj == 0){
            		rysuj = 1;
            		for(int i=0;i<5;i++){
            			com_talia.get(i).losujMoc();
            		}
            		for(int i=0;i<5;i++){
            			player_talia.get(i).losujMoc();
            		}
            		paint2();
            	}
            	else
            	{
					JOptionPane.showMessageDialog(null, "Trwa rozgrywka.");
            	}
            }
        });
		
		setBounds(0,0,SZEROKOSC,WYSOKOSC);
		panel.setPreferredSize(new Dimension(SZEROKOSC,WYSOKOSC));
		panel.setLayout(null);
		panel.add(this);
		okno.setBounds(0,0,SZEROKOSC,WYSOKOSC);
		okno.setVisible(true);
		okno.setResizable(false);
		okno.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		});
		createBufferStrategy(2);
		strategia = getBufferStrategy();
		requestFocus();
		
		//Uzyskanie informacji o karcie po wcisnieciu
		addMouseListener(new MouseAdapter(){
	        public void mouseClicked(MouseEvent e) {
	    		Point point = e.getPoint();
	    		int x = 69;
	    		int y = 80;
	    		for(int i=0;i<com_talia.size();i++){
	    			Rectangle imageBounds = new Rectangle((2*i+1)*x,y,96, 96);
	    			if(imageBounds.contains(point)) {
	    				JOptionPane.showMessageDialog(null, "Moc tej karty: "+com_talia.get(i).getMocL()+"-"+com_talia.get(i).getMocP()+"\nRanga: "+(i+1)+"/5");
	    			}
	    		}
	    		for(int i=0;i<player_talia.size();i++){
	    			Rectangle imageBounds = new Rectangle((2*i+1)*x,3*y,96, 96);
	    			if(imageBounds.contains(point)) {
	    				JOptionPane.showMessageDialog(null, "Moc karty: "+player_talia.get(i).getMocL()+"-"+player_talia.get(i).getMocP()+"\nRanga: "+(i+1)+"/5");
	    			}
	    		}
	        }
	    });
	}
	
	public void paint2(){
		Graphics g = strategia.getDrawGraphics();
		g.setColor(Color.BLACK);
		Font f = new Font("Impact", Font.PLAIN, 40);
		g.setFont(f);
		g.fillRect(0,0,getWidth(),getHeight());
		g.drawImage(getSprite("background1.png"), 0, 0,this);

		int x = 69;
		int y = 80;
		
		if(rysuj != 0){
			g.drawImage(getSprite("light.png"), (2*(rysuj-1)+1)*x+2, 0,this);
			g.drawString(String.valueOf(com_talia.get(rysuj-1).getMoc()), (2*(rysuj-1)+1)*x+28-((rysuj-1)*2), y-10);
			g.drawString(String.valueOf(player_talia.get(rysuj-1).getMoc()), (2*(rysuj-1)+1)*x+28-((rysuj-1)*2), 3*y-10);
		}

		g.drawString(String.valueOf(com_pkts), 127, 548);
		g.drawString(String.valueOf(player_pkts), 733, 548);
		
		for(int i=0;i<com_talia.size();i++){
			g.drawImage(resize(getSprite(com_talia.get(i).getNazwa()+".png"), 96, 96), (2*i+1)*x, y,this);
			
		}
		for(int i=0;i<player_talia.size();i++){
			g.drawImage(resize(getSprite(player_talia.get(i).getNazwa()+".png"), 96, 96), (2*i+1)*x, 3*y,this);	
		}
		strategia.show();
	}
	 
	public void wczytajTalie(int n, ArrayList<Karta> talia){
		try {
			File file = new File("talia" + n + ".txt");
			Scanner wczytaj = new Scanner(file);
			int iloscKart = 0;
			if(wczytaj.hasNextInt()){
				iloscKart = wczytaj.nextInt();
			}
			for(int i=0;i<iloscKart;i++){
				int mocl = 0;
				int mocp = 0;
				String nazwa = n + "_" + (i+1);
				mocl = wczytaj.nextInt();
				mocp = wczytaj.nextInt();
				talia.add(new Karta(mocl, mocp));
				talia.get(talia.size()-1).setNazwa(nazwa);
			}
			wczytaj.close();
		}
		catch (Exception e){
			System.out.println("Nie mozna wczytac pliku");
		};
	}
	
	public void losujKarte(){
		int los = 0;
		Random losuj = new Random();
		los = losuj.nextInt(talia1.size()-2);
		com_talia.add(talia1.get(los));
		int los2 = 0;
		while((los2 = losuj.nextInt(talia1.size()-2)) == los){}
		player_talia.add(talia1.get(los2));
	
		los = losuj.nextInt(talia2.size()-2);
		com_talia.add(talia2.get(los));
		while((los2 = losuj.nextInt(talia2.size()-2)) == los){}
		player_talia.add(talia2.get(los2));
		
		los = losuj.nextInt(talia3.size()-2);
		com_talia.add(talia3.get(los));
		while((los2 = losuj.nextInt(talia3.size()-2)) == los){}
		player_talia.add(talia3.get(los2));
		
		los = losuj.nextInt(talia4.size()-2);
		com_talia.add(talia4.get(los));
		while((los2 = losuj.nextInt(talia4.size()-2)) == los){}
		player_talia.add(talia4.get(los2));
		
		los = losuj.nextInt(talia5.size()-2);
		com_talia.add(talia5.get(los));
		while((los2 = losuj.nextInt(talia5.size()-2)) == los){}
		player_talia.add(talia5.get(los2));
	}
	
	public void graj(){
		while(isVisible()){
			if(rysuj >= 1 && rysuj <= 5){
				if(com_talia.get(rysuj-1).getMoc() > player_talia.get(rysuj-1).getMoc()){
					com_pkts++;
				}
				else if(com_talia.get(rysuj-1).getMoc() == player_talia.get(rysuj-1).getMoc()){
					com_talia.get(rysuj-1).losujMoc();
					player_talia.get(rysuj-1).losujMoc();
					rysuj--;
				}
				else
				{
					player_pkts++;
					wygrana++;
				}
				SZYBKOSC = 3000;
				try{
					Thread.sleep(SZYBKOSC);
				}
				catch(InterruptedException e){};
				rysuj++;
				if(rysuj == 6){
					if(wygrana >= 3){
						player_win++;
						JOptionPane.showMessageDialog(null, "Runda wygrana! "+wygrana+":"+(5-wygrana));
						if(player_win == 1){
							if(zlota1 == false){
								player_talia.add(0, talia1.get(talia1.size()-1));
								zlota1 = true;
							}
							else
								player_talia.add(0, talia1.get(talia1.size()-2));
						}
						else if(player_win == 2){
							if(zlota2 == false){
								player_talia.add(1, talia2.get(talia2.size()-1));
								zlota2 = true;
							}
							else
								player_talia.add(0, talia2.get(talia2.size()-2));
						}
						else if(player_win == 3){
							if(zlota3 == false){
								player_talia.add(2, talia3.get(talia3.size()-1));
								zlota3 = true;
							}
							else
								player_talia.add(2, talia3.get(talia3.size()-2));
						}
						else if(player_win == 4){
							if(zlota4 == false){
								player_talia.add(3, talia4.get(talia4.size()-1));
								zlota4 = true;
							}
							else
								player_talia.add(3, talia4.get(talia4.size()-2));
						}
						else if(player_win == 5){
							if(zlota5 == false){
								player_talia.add(4, talia5.get(talia5.size()-1));
								zlota5 = true;
							}
							else
								player_talia.add(4, talia5.get(talia5.size()-2));
						}

						player_talia.remove(player_win);
						if(player_win == 5){
							JOptionPane.showMessageDialog(null, "WYGRANA!");
						}
					}
					else
					{
						com_win++;
						JOptionPane.showMessageDialog(null, "Runda przegrana. "+(5-wygrana)+":"+(wygrana));
						if(com_win == 1){
							if(zlota1 == false){
								com_talia.add(0, talia1.get(talia1.size()-1));
								zlota1 = true;
							}
							else
								com_talia.add(0, talia1.get(talia1.size()-2));
						}
						else if(com_win == 2){
							if(zlota2 == false){
								com_talia.add(1, talia2.get(talia2.size()-1));
								zlota2 = true;
							}
							else
								com_talia.add(1, talia2.get(talia2.size()-2));
						}
						else if(com_win == 3){
							if(zlota3 == false){
								com_talia.add(2, talia3.get(talia3.size()-1));
								zlota3 = true;
							}
							else
								com_talia.add(2, talia3.get(talia3.size()-2));
						}
						else if(com_win == 4){
							if(zlota4 == false){
								com_talia.add(3, talia4.get(talia4.size()-1));
								zlota4 = true;
							}
							else
								com_talia.add(3, talia4.get(talia4.size()-2));
						}
						else if(com_win == 5){
							if(zlota5 == false){
								com_talia.add(4, talia5.get(talia5.size()-1));
								zlota5 = true;
							}
							else
								com_talia.add(4, talia5.get(talia5.size()-2));
						}

						com_talia.remove(com_win);
						if(com_win == 5){
							JOptionPane.showMessageDialog(null, "Przegrana.");
						}
					}
					wygrana = 0;
					rysuj = 0;
					SZYBKOSC = 60;
				}
			}
			paint2();
		}
	}
	
	public static void main(String[] args){
		Gra animeCard = new Gra();
		
		//Wypisanie pierwszej talii
		ListIterator<Karta> it = talia1.listIterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		//Sprawdzenie dzialania wyjatku
		try {
			wypiszKarteZTalii(talia1, 2);
		}
		catch (NonExistCardException e) { System.out.println("podana karta nie istnieje.."); }
	
		try {
			wypiszKarteZTalii(talia1, 20);
		}
		catch (NonExistCardException e) { System.out.println("podana karta nie istnieje.."); }
		
		//Uzycie polimorfizmu
		Karta[] tab = new Karta[2];
		tab[0] = new Karta(10, 20);
		tab[1] = new ZwyklaKarta(5, 10);
		for(int i=0;i<2;i++){
			System.out.println(tab[i]);
		}
		animeCard.graj();
		
	}	

}