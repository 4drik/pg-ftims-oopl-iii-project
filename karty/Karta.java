package karty;

import java.util.Random;

public class Karta {

	private int mocl;
	private int mocp;
	private int moc;
	private String nazwa_obrazka = "";
	
	public Karta(int lewa, int prawa){
		setMoc(lewa, prawa);
	}
	
	public void setMoc(int lewa, int prawa){
		mocl = lewa;
		mocp = prawa;
	}
	public void setWylosowanaMoc(int moc){
		this.moc = moc;
	}
	public void setNazwa(String nazwa){
		nazwa_obrazka = nazwa;
	}
	public String getNazwa(){
		return nazwa_obrazka;
	}
	public int getMoc(){
		return moc;
	}
	//Przedzial mocy (od lewej, mniejszej do prawej, wiekszej)
	public int getMocL(){
		return mocl;
	}
	public int getMocP(){
		return mocp;
	}
	
	public void losujMoc(){
		Random losuj = new Random();
		moc = losuj.nextInt(mocp-mocl)+mocl;
	}
	
	public String toString(){
		return "mocl = " + mocl + " mocp = " + mocp;
	}
}
