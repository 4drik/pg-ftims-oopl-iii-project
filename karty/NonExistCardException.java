package karty;

public class NonExistCardException extends Exception {
	@Override
	public String getMessage() {
		return "WARNING: card doesn't exist!";
	}
}
