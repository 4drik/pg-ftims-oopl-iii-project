package karty;

public class ZwyklaKarta extends Karta implements Dodatek1, Dodatek2 {

	public ZwyklaKarta(int lewa, int prawa) {
		super(lewa, prawa);
	}

	private int id;
	
	public String toString(){
		return super.toString();
	}
	
	@Override
	public int wypiszPrzedzial(){
		return (getMocP()-getMocL());
	}
	@Override
	public int wypiszMaksymalnaMoc(){
		return getMocP();
	}
}
