package karty;

import java.util.Random;

public class SuperKarta extends ZwyklaKarta {
	public SuperKarta(int lewa, int prawa) {
		super(lewa, prawa);
	}
	private int criticalChance = 10;
	public boolean isCritical(){
		Random losuj = new Random();
		if(losuj.nextInt(100) < criticalChance){
			return true;
		}
		else
		{
			return false;
		}
	}
}
